//
//  Lifesign_ModelsTests.swift
//  Lifesign-ModelsTests
//
//  Created by Peter Rosendahl on 18/04/2019.
//  Copyright © 2019 Peter Rosendahl. All rights reserved.
//

import XCTest
@testable import Lifesign_Models

class Lifesign_ModelsTests: XCTestCase {
    let dm = DeviceMuck.init()
    let cm = CloudMuck.init()

    func testUnknown() {
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: cm)
        
        let unknown = model.userUnknown()
        XCTAssert(unknown)
    }
    
    func testBasicSaveAndLoad() {
        let u1 = User.init()
        u1.userId = "U-0000-0000-1"
        u1.userName = "Carsten xXx"
        u1.email = "C@xxx.dk"
        u1.cellno = "2211221122"
        u1.platform = PlatformType.iOS

        let localSave = dm.savelocalUser(user: u1)
        XCTAssert(localSave)
        
        let u2 = dm.loadlocalUser()
        XCTAssert(u1.userId == u2?.userId)
    }
    
    func testModelSaveAndLoad() {
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: cm)
        
        let u3 = User.init()
        u3.userName = "Erik the Weak"
        u3.email = "erik@weak.dk"
        u3.cellno = "2211221177"
        u3.platform = PlatformType.iOS

        let modelsave = model.saveUserParameters(userName: u3.userName, email: u3.email, cellno: u3.cellno, platform: u3.platform)
        XCTAssert(modelsave)
        
        model.initUser() // indlæs den gemte user
        
        let unknown = model.userUnknown()
        XCTAssert(unknown == false)
        
        XCTAssert(u3.userName == model.user!.userName)
    }
    
    func testLoadKnown() {
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: cm)
        let userid = "U-0000-0000-0004"
        dm.user = User.init()
        dm.user!.userId = userid
        dm.user!.userName = "Bruno den klamme"
        dm.user!.email = "bruno@klamme.dk"
        dm.user!.cellno = "2244221177"
        dm.user!.platform = PlatformType.iOS
        
        model.initUser()

        let unknown = model.userUnknown()
        XCTAssert(unknown == false)
        
        XCTAssert(userid == model.user!.userId)
    }

    func testLoadKnownOtherFromCloud() {
        let model = Model.init(deviceLayerImplementation: dm, cloudLayerImplementation: cm)
        
        let userid = "U-0000-0000-0004"
        dm.user = User.init()
        dm.user!.userId = userid
        dm.user!.userName = "Bruno den klamme"
        dm.user!.email = "bruno@klamme.dk"
        dm.user!.cellno = "2244221177"
        dm.user!.platform = PlatformType.iOS

        let nytNavn = "Bruno den lækre"
        cm.user = User.init()
        cm.user!.userId = userid
        cm.user!.userName = nytNavn
        cm.user!.email = "bruno@klamme.dk"
        cm.user!.cellno = "2244221177"
        cm.user!.platform = PlatformType.iOS

        model.initUser()
        
        let unknown = model.userUnknown()
        XCTAssert(unknown == false)
        
        XCTAssert(model.user!.userName == nytNavn)
    }
}

class CloudMuck: CloudLayer {
    var user : User?

    func postUser(user: User) -> User {
        let user = user
        if (user.userId == "") {
            user.userId = "U-" + UUID().uuidString
        }
        
        return user
    }
    
    func getUser(userid: String) -> User? {

        let user = self.user
        user?.userId = userid
        
        return user
    }
}
    
class DeviceMuck: DeviceLayer {
    var user : User?
    
    func savelocalUser(user: User) -> Bool {
        self.user = user
        
        return true
    }
    
    func loadlocalUser() -> User? {
        return user
    }
}
