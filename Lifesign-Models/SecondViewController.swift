//
//  SecondViewController.swift
//  Lifesign-Models
//
//  Created by Peter Rosendahl on 18/04/2019.
//  Copyright © 2019 Peter Rosendahl. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    var model = (UIApplication.shared.delegate as! AppDelegate).model

    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfCellno: UITextField!
    @IBOutlet weak var scPlatform: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (model.userUnknown()) {
            tfName.text = ""
            tfEmail.text = ""
            tfCellno.text = ""
            scPlatform.selectedSegmentIndex = 0
        }
        else {
            tfName.text = model.user!.userName
            tfEmail.text = model.user!.email
            tfCellno.text = model.user!.cellno
            scPlatform.selectedSegmentIndex = model.user!.platform.hashValue
        }
    }

    @IBAction func btnSaveAndSync(_ sender: Any) {
        let platformEnum = PlatformType(rawValue: scPlatform.selectedSegmentIndex)

        _ = model.saveUserParameters(userName: tfName.text!, email: tfEmail.text!, cellno: tfCellno.text!, platform: platformEnum ?? PlatformType.iOS)
        
        let tabBar: UITabBarController = (UIApplication.shared.delegate as! AppDelegate).window!.rootViewController as! UITabBarController
        tabBar.selectedIndex = 0
    }
    
}

