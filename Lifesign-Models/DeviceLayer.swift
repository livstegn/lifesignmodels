//
//  DeviceLayer.swift
//  Lifesign-Models
//
//  Created by Peter Rosendahl on 19/04/2019.
//  Copyright © 2019 Peter Rosendahl. All rights reserved.
//

import Foundation

protocol DeviceLayer {
    func savelocalUser(user: User) -> Bool
    func loadlocalUser() -> User?
}

class DeviceLayerImplementation: DeviceLayer {
    let localUserFilename = "user"
    let xmlExtension = "xml"

    func savelocalUser(user: User) -> Bool
    {
        var result = false
        
        let dir = try? FileManager.default.url(for: .documentDirectory,
                                               in: .userDomainMask, appropriateFor: nil, create: true)
        
        if let fileURL = dir?.appendingPathComponent(localUserFilename).appendingPathExtension(xmlExtension)
        {
            let encoder = PropertyListEncoder()
            encoder.outputFormat = .xml
            do {
                let data = try encoder.encode(user)
                try data.write(to: fileURL)
                result = true
            } catch {
                print(error)
            }
        }
        
        return result
    }
    
    
    func loadlocalUser() -> User?
    {
        var user : User? = nil
        
        let dir = try? FileManager.default.url(for: .documentDirectory,
                                               in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = dir?.appendingPathComponent(localUserFilename).appendingPathExtension(xmlExtension)
        if fileURL != nil
        {
            //print("loading user from", fileURL)
            if let data = try? Data(contentsOf: fileURL!) {
                let decoder = PropertyListDecoder()
                user = try! decoder.decode(User.self, from: data)
            }
        }
        
        return user
    }
}
