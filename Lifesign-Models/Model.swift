//
//  LifesignModel.swift
//  Lifesign-Model
//
//  Created by Peter Rosendahl on 18/04/2019.
//  Copyright © 2019 Peter Rosendahl. All rights reserved.
//

import Foundation

class Model {
    let localUserFilename = "user"
    let xmlExtension = "xml"
    var deviceLayer: DeviceLayer
    var cloudLayer: CloudLayer
    var user : User?
    
    init (deviceLayerImplementation: DeviceLayer, cloudLayerImplementation: CloudLayer) {
        self.deviceLayer = deviceLayerImplementation
        self.cloudLayer = cloudLayerImplementation
    }
    
    func initUser() {
        user = deviceLayer.loadlocalUser()
        
        if (user != nil) {
            if (user!.userId != "") {
                let remoteUser = cloudLayer.getUser(userid: user!.userId)
                if (remoteUser != nil) {
                    user = remoteUser
                }
            }
        }
    }
    
    func userUnknown() -> Bool
    {
        if user == nil {
            return true
        }
        else {
            return false
        }
    }
    
    func saveUserParameters(userName: String, email: String, cellno: String, platform: PlatformType) -> Bool {
        if self.userUnknown() {
            user = User.init()
        }
        
        user!.userName = userName
        user!.email = email
        user!.cellno = cellno
        user!.platform = platform

        // Save the user in cloud, obtain key an other stuff before local save
        let cloudUser = cloudLayer.postUser(user: user!)

        let localSave = deviceLayer.savelocalUser(user: cloudUser)
        
        return localSave
    }
}

