//
//  CloudLayer.swift
//  Lifesign-Models
//
//  Created by Peter Rosendahl on 19/04/2019.
//  Copyright © 2019 Peter Rosendahl. All rights reserved.
//

import Foundation

protocol CloudLayer {
    func postUser(user: User) -> User
    func getUser(userid: String) -> User?
}

class CloudLayerImplementation: CloudLayer {
    let localUserFilename = "user"

    // POST /users {?name="..."&email="..."<&cellno="..."><&deviceid="..."><&platform="...">}
    func postUser(user: User) -> User {
        let user = user
        return user
    }
    
    func getUser(userid: String) -> User? {
        let user = User.init()
        return user
    }
}
