//
//  FirstViewController.swift
//  Lifesign-Models
//
//  Created by Peter Rosendahl on 18/04/2019.
//  Copyright © 2019 Peter Rosendahl. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    var model = (UIApplication.shared.delegate as! AppDelegate).model
    
    @IBOutlet weak var lblUsername: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (model.userUnknown()) {
            lblUsername.text = "No user yet..."
        }
        else {
            lblUsername.text = model.user!.userName
        }
    }


}

