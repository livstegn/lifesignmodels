//
//  LifesignUser.swift
//  Lifesign-Model
//
//  Created by Peter Rosendahl on 18/04/2019.
//  Copyright © 2019 Peter Rosendahl. All rights reserved.
//

import Foundation

class User : NSObject, Codable  {
    var userId : String
    var userName: String
    var email : String
    var cellno : String
    var status: UserStatusType
    var deviceId : String
    var platform: PlatformType
    
    enum CodingKeys: String, CodingKey
    {
        case userId = "userId"
        case userName = "userName"
        case email = "email"
        case cellno = "cellno"
        case status = "status"
        case deviceId = "deviceId"
        case platform = "platform"
    }
    
    override init() {
        userId = ""
        userName = "John Doe"
        email = ""
        cellno = ""
        status = UserStatusType.Creating
        deviceId = ""
        platform = PlatformType.iOS
    }
}

enum UserStatusType : Int, Codable {
    case Invited = 0, Creating, Created, Active, Closing
    
    static var count: Int { return UserStatusType.Closing.hashValue + 1 }
    
    var description: String {
        switch self {
        case .Invited: return "Invited"
        case .Creating : return "Creating"
        case .Created : return "Created"
        case .Active : return "Active"
        case .Closing : return "Closing"
        }
    }
}

enum PlatformType : Int, Codable {
    case iOS = 0, Android, Browser
    
    static var count: Int { return PlatformType.Browser.hashValue + 1 }
    
    var description: String {
        switch self {
        case .iOS: return "iOS"
        case .Android : return "Android"
        case .Browser : return "Browser"
        }
    }
}
